﻿using System.Collections.Generic;

using UnityEngine;

public sealed class Enemy : MonoBehaviour
{
    private static readonly List<Enemy> all = new List<Enemy>();

    public static void DestroyAll()
    {
        for (int i = Enemy.all.Count - 1; i >= 0; --i)
        {
            GameObject.Destroy(Enemy.all[i].gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        Player otherPlayer = otherCollider.gameObject.GetComponent<Player>();
        if (otherPlayer != null)
        {
            otherPlayer.Die(false);
        }
    }

    private void OnEnable()
    {
        Enemy.all.Add(this);
    }

    private void OnDisable()
    {
        Enemy.all.Remove(this);
    }
}
