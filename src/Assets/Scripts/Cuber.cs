﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cuber : MonoBehaviour {

    [SerializeField]
    private float speed = 1.0f;

    [SerializeField]
    private Color color;

    [SerializeField]
    private Renderer[] cubes;

    [SerializeField]
    private float[] alphas;

	// Use this for initialization
	void Start () {
        for(int i = 0; i < cubes.Length; i++)
        {
            cubes[i].material.SetColor("_EmissionColor", this.color * this.alphas[i]);
        }
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position += new Vector3(-1.0f, 0f, 0f) * this.speed * Time.deltaTime;
	}
}
