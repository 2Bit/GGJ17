﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Slerpy.Unity3D;

public sealed class Wave : MonoBehaviour
{
    [SerializeField]
    private int stepsPerSecond = 5;

    [SerializeField]
    private float scaleX = 1.0f;

    [SerializeField]
    private float scaleY = 1.0f;

    [SerializeField]
    private LineRenderer lineRenderer = null;

    [SerializeField]
    private EdgeCollider2D edgeCollider = null;

    [SerializeField]
    private Camera view = null;

    [SerializeField]
    private Renderer[] viewBackgrounds = null;

    [SerializeField]
    private AudioSource audioSource = null;

    [SerializeField]
    private GameObject[] lowSpawnPrefabs = null;

    [SerializeField]
    private GameObject[] highSpawnPrefabs = null;

    [SerializeField]
    private Effect generatedEffect = null;

    private float spawnTimeRemaining = 3.0f;

    private bool hasPlayed = false;
    private float endTime = 0.0f;

    private float[] steps = null;
    private int lastChunkID = -1;

    public bool IsGenerated
    {
        get
        {
            return this.steps != null;
        }
    }

    private IEnumerator LoadClip(string path)
    {
        var musicFile = new WWW("file:///" + path);
        yield return musicFile;
        this.Generate(musicFile.audioClip);
    }

    public void Awake()
    {
        this.StartCoroutine(this.LoadClip(MusicLoader.CurrentMusicPath));
    }

    public bool IsEnding
    {
        get
        {
            return this.endTime > 0.0f;
        }
    }

    public bool IsEnded
    {
        get
        {
            return this.endTime >= 5.0f;
        }
    }

    public void Play()
    {
        this.audioSource.Play();
    }

    public void Pause()
    {
        this.audioSource.Pause();
    }

    public void Rewind()
    {
        this.audioSource.time = 0.0f;

        this.endTime = 0.0f;
        this.hasPlayed = false;
    }

    public void Generate(AudioClip audio)
    {
        this.audioSource.clip = audio;
        float[] data = new float[this.audioSource.clip.samples * this.audioSource.clip.channels];
        this.audioSource.clip.GetData(data, 0);

        float[] dataLeft = new float[data.Length / this.audioSource.clip.channels];
        for (int i = 0; i < dataLeft.Length; ++i)
        {
            dataLeft[i] = data[i * this.audioSource.clip.channels];
        }

        List<float> steps = new List<float>();
        for (float i = 0; i < this.audioSource.clip.length; i += (1.0f / this.stepsPerSecond))
        {
            float value = dataLeft[(int)(dataLeft.Length * (i / this.audioSource.clip.length))];

            value = Mathf.Abs(value);

            float newStep = value * this.scaleY;

            steps.Add(newStep);
        }

        float[] oldSteps = steps.ToArray();
        for (int i = 0; i < steps.Count; ++i)
        {
            float value = 0.0f;
            
            for (int k = -7; k < 8; ++k)
            {
                int index = i + k;
                if (i + k < 0)
                {
                    index = 0;
                }

                if (i + k >= steps.Count)
                {
                    index = steps.Count - 1;
                }

                value += oldSteps[index] * (1.0f / 15);
            }

            steps[i] = value;
        }

        oldSteps = steps.ToArray();
        for (int i = 0; i < steps.Count; ++i)
        {
            float value = 0.0f;
            
            for (int k = -3; k < 4; ++k)
            {
                int index = i + k;
                if (i + k < 0)
                {
                    index = 0;
                }

                if (i + k >= steps.Count)
                {
                    index = steps.Count - 1;
                }

                value += oldSteps[index] * (1.0f / 7);
            }

            steps[i] = value;
        }

        float largestStep = 0.0f;

        for (int i = 0; i < steps.Count; ++i)
        {
            if (steps[i] > largestStep)
            {
                largestStep = steps[i];
            }
        }

        largestStep = this.scaleY / largestStep;

        for (int i = 0; i < steps.Count; ++i)
        {
            steps[i] *= largestStep;
        }

        this.steps = steps.ToArray();

        this.Rewind();
        this.Construct();

        this.generatedEffect.PlayForward();
    }

    [ContextMenu("Generate from Source")]
    private void GenerateFromSource()
    {
        this.Generate(this.audioSource.clip);
    }

    private void Construct()
    {
        int chunkSize = this.stepsPerSecond * 10;

        int currentIndex = Mathf.Max(0, (int)(this.audioSource.time * this.stepsPerSecond));
        int maxIndex = chunkSize;
        int chunkIndex = currentIndex % maxIndex;

        if (currentIndex >= chunkSize)
        {
            chunkIndex += chunkSize;
            maxIndex += chunkSize;
        }

        if (maxIndex - chunkIndex > this.steps.Length - 1 - currentIndex)
        {
            maxIndex = chunkIndex + (this.steps.Length - 1 - currentIndex);
        }

        float xStart = this.view.ViewportToWorldPoint(new Vector3(1.0f, 0.5f, 0.0f)).x - (chunkIndex / (float)this.stepsPerSecond) * this.scaleX;

        Vector2[] points = new Vector2[maxIndex + 4];
        this.lineRenderer.numPositions = maxIndex + 4;

        points[0] = new Vector2(xStart - 100.0f, 1.5f);
        this.lineRenderer.SetPosition(0, new Vector3(xStart - 10000.0f, 1.5f, 0.0f));

        points[1] = new Vector2(xStart + 0.0f, 1.5f);
        this.lineRenderer.SetPosition(1, new Vector3(xStart + 0.0f, 1.5f, 0.0f));

        for (int i = 0; i < maxIndex; ++i)
        {
            Vector2 point = new Vector2(xStart + (i / (float)this.stepsPerSecond) * this.scaleX, this.steps[currentIndex - chunkIndex + i]);

            points[i + 2] = point;
            this.lineRenderer.SetPosition(i + 2, new Vector3(point.x, point.y, 0.0f));
        }

        points[maxIndex + 2] = new Vector2(xStart + (maxIndex / (float)this.stepsPerSecond) * this.scaleX + 0.0f, 1.5f);
        this.lineRenderer.SetPosition(maxIndex + 2, new Vector3(xStart + (maxIndex / (float)this.stepsPerSecond) * this.scaleX + 0.0f, 1.5f, 0.0f));

        points[maxIndex + 3] = new Vector2(xStart + (maxIndex / (float)this.stepsPerSecond) * this.scaleX + 10000.0f, 1.5f);
        this.lineRenderer.SetPosition(maxIndex + 3, new Vector3(xStart + (maxIndex / (float)this.stepsPerSecond) * this.scaleX + 10000.0f, 1.5f, 0.0f));

        this.edgeCollider.points = points;

        this.lastChunkID = currentIndex / chunkSize;
    }

    private void Update()
    {
        if (this.IsGenerated)
        {
            if (this.audioSource.time > 0.0f)
            {
                this.hasPlayed = true;
            }

            if (this.hasPlayed && this.audioSource.time <= 0.0f)
            {
                this.endTime += Time.deltaTime;
            }

            float time = (this.IsEnding ? this.audioSource.clip.length : this.audioSource.time) + this.endTime;

            float xPosition = time * this.scaleX;

            this.view.transform.position = new Vector3(xPosition, 0.0f, this.view.transform.localPosition.z);

            for (int i = 0; i < this.viewBackgrounds.Length; ++i)
            {
                this.viewBackgrounds[i].material.SetTextureOffset("_MainTex", new Vector2(20.0f - time * (0.1f - i * 0.01f), 20.0f));
            }

            if (!this.IsEnding)
            {
                int chunkSize = this.stepsPerSecond * 10;

                int currentIndex = Mathf.Max(0, (int)(this.audioSource.time * this.stepsPerSecond));
                int chunkIndex = (currentIndex % chunkSize) + 2;

                if (this.lastChunkID != currentIndex / chunkSize)
                {
                    this.Construct();
                }

                float nextWeight = (this.audioSource.time % (1.0f / this.stepsPerSecond)) * this.stepsPerSecond;

                float yPosition = Vector3.Lerp(
                    this.lineRenderer.GetPosition(chunkIndex),
                    this.lineRenderer.GetPosition(Mathf.Min(this.lineRenderer.numPositions - 1, chunkIndex + 1)),
                    nextWeight).y;

                float scaledYPosition = yPosition / this.scaleY;

                this.spawnTimeRemaining -= Time.deltaTime * scaledYPosition;
                if (this.spawnTimeRemaining <= 0.0f)
                {
                    GameObject spawnedObject = GameObject.Instantiate(
                        scaledYPosition > 0.5f ? this.highSpawnPrefabs[Random.Range(0, this.highSpawnPrefabs.Length)] : this.lowSpawnPrefabs[Random.Range(0, this.lowSpawnPrefabs.Length)],
                        new Vector3(this.view.ViewportToWorldPoint(new Vector3(1.0f, 0.5f, 0.0f)).x + 5.0f, Random.Range(-3.0f, 1.0f), 0.0f),
                        Quaternion.identity);

                    GameObject.Destroy(spawnedObject, 20.0f);

                    this.spawnTimeRemaining += 0.5f + 0.75f * (1.0f - (this.audioSource.time / this.audioSource.clip.length));
                }
            }
        }
    }
}
