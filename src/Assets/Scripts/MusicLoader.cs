﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MusicLoader : MonoBehaviour {
    private static string musicPath;
    private readonly string[] validExtensions = { ".ogg", ".wav", ".OGG", ".WAV" };

    public static string CurrentMusicPath;

    [SerializeField]
    private Text songText;

    Dictionary<string, string> musicFiles = new Dictionary<string, string>();

    private List<string> musicNames = new List<string>();

    private int musicIndex;

    private int MusicIndex
    {
        get
        {
            return this.musicIndex;
        }
        set
        {
            this.musicIndex = value;
            songText.text = musicNames.Count > 0 ? musicNames[this.musicIndex].TrimStart('_') : "No songs found";
            CurrentMusicPath = this.GetMusicPath();
        }
    }

    void Awake()
    {
        musicPath = Application.dataPath + "/StreamingAssets/Audio/";
        var directory = new DirectoryInfo(musicPath);
        foreach (var file in directory.GetFiles())
        {
            if (Array.IndexOf(validExtensions, file.Extension) != -1)
            {
                string name = file.Name.TrimEnd(file.Extension.ToCharArray());
                musicFiles[name] = file.FullName;
                musicNames.Add(name);
            }
        }
        musicNames.Sort();
        MusicIndex = 0;
    }

    public void NextSong()
    {
        if(musicNames.Count < 1)
        {
            return;
        }
        MusicIndex = (MusicIndex + 1) % this.musicNames.Count;
    }

    public void PrevSong()
    {
        if (musicNames.Count < 1)
        {
            return;
        }
        MusicIndex = (MusicIndex - 1) < 0 ? this.musicNames.Count - 1 : MusicIndex - 1;
    }

    public string GetMusicPath()
    {
        return this.musicFiles[this.musicNames[this.MusicIndex]];
    }
}
