﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public sealed class Grapple : MonoBehaviour
{
    [SerializeField]
    private float throwForce = 5.0f;

    [SerializeField]
    private SpringJoint2D joint = null;

    [SerializeField]
    private LineRenderer line = null;

    private Transform retractParent = null;

    public bool IsGrappling
    {
        get
        {
            return this.retractParent != null;
        }
    }

    public float GrappleLength
    {
        get
        {
            return this.joint.distance;
        }

        set
        {
            this.joint.distance = Mathf.Max(0.2f, value);
        }
    }

    public bool IsThrowing
    {
        get
        {
            return !this.Rigidbody2D.isKinematic;
        }
    }

    public Rigidbody2D Rigidbody2D { get; private set; }

    public void Throw(Vector3 direction)
    {
        this.Rigidbody2D.isKinematic = false;
        this.Rigidbody2D.AddForce(direction * this.throwForce, ForceMode2D.Impulse);

        this.joint.enabled = false;
    }

    public void Hold()
    {
        this.Rigidbody2D.isKinematic = true;
        this.Rigidbody2D.velocity = Vector3.zero;

        this.joint.enabled = true;
    }

    public void Retract()
    {
        if (this.retractParent != null)
        {
            this.transform.parent = this.retractParent;

            this.retractParent = null;
        }

        this.transform.localPosition = Vector3.zero;

        this.Hold();

        this.joint.enabled = false;
    }

    private void Awake()
    {
        this.Rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();

        this.Retract();
    }

    private void LateUpdate()
    {
        this.line.SetPosition(0, this.transform.position);
        this.line.SetPosition(1, (this.retractParent == null ? this.transform.parent : this.retractParent).position);
    }

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (this.transform.parent != null)
        {
            this.GrappleLength = this.transform.localPosition.magnitude * 0.8f;

            this.retractParent = this.transform.parent;
            this.transform.parent = otherCollider.transform;

            this.Hold();
        }
    }
}
