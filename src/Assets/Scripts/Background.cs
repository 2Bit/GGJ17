﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    [SerializeField]
    private Material bMaterial;

    [SerializeField]
    private float scrollSpeed = 1.0f;

    private float textureOffset = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.textureOffset += (Time.deltaTime * this.scrollSpeed) % 1.0f;
        this.bMaterial.SetTextureOffset("_MainTex", new Vector2(0f, this.textureOffset));
    }
}
