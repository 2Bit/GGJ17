﻿using UnityEngine;
using UnityEngine.UI;

using Slerpy.Unity3D;

[RequireComponent(typeof(Rigidbody2D))]
public sealed class Player : MonoBehaviour
{
    [SerializeField]
    private Grapple grapple = null;

    [SerializeField]
    private Wave wave = null;

    [SerializeField]
    private Camera view = null;

    [SerializeField]
    private Effect[] despawnEffects = null;

    [SerializeField]
    private AudioSource[] deathSounds = null;

    [SerializeField]
    private UnityStandardAssets.ImageEffects.Twirl deathPostProcess = null;

    [SerializeField]
    private Effect[] pauseEffects = null;

    [SerializeField]
    private UnityStandardAssets.ImageEffects.Grayscale pausePostProcess = null;

    [SerializeField]
    private Effect[] pauseAndEndEffects = null;

    [SerializeField]
    private Effect[] endEffects = null;

    [SerializeField]
    private Text endText = null;

    private bool isPaused = false;

    private float invincibleTime = 0.0f;
    private float dyingTime = 0.0f;

    public int DeathCount { get; private set; }

    public bool IsPaused
    {
        get
        {
            return this.isPaused;
        }

        private set
        {
            if (this.isPaused != value)
            {
                this.isPaused = value;

                if (this.isPaused)
                {
                    Time.timeScale = 0.0f;
                    this.wave.Pause();

                    for (int i = 0; i < this.pauseEffects.Length; ++i)
                    {
                        this.pauseEffects[i].PlayForward();
                    }
                }
                else
                {
                    Time.timeScale = 1.0f;

                    if (!this.wave.IsEnding)
                    {
                        this.wave.Play();
                    }

                    for (int i = 0; i < this.pauseEffects.Length; ++i)
                    {
                        this.pauseEffects[i].PlayBackward();
                    }
                }
            }
        }
    }

    public bool IsInvincible
    {
        get
        {
            return this.invincibleTime > 0.0f;
        }
    }

    public bool IsDying
    {
        get
        {
            return this.dyingTime > 0.0f;
        }
    }

    public Rigidbody2D Rigidbody2D { get; private set; }

    public void Die(bool force)
    {
        if (!this.IsDying && (force || !this.IsInvincible))
        {
            this.dyingTime = 2.0f;
            this.invincibleTime = 1.0f;

            this.IsPaused = true;

            this.grapple.Retract();
            this.Rigidbody2D.velocity = Vector3.zero;

            if (!this.wave.IsEnded)
            {
                for (int i = 0; i < this.deathSounds.Length; ++i)
                {
                    this.deathSounds[i].Play();
                }
            }

            for (int i = 0; i < this.despawnEffects.Length; ++i)
            {
                this.despawnEffects[i].PlayForward();
            }

            if (!this.wave.IsEnded)
            {
                ++this.DeathCount;
            }
        }
    }

    private void Awake()
    {
        this.Rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        this.IsPaused = true;
    }

    private void Update()
    {
        this.deathPostProcess.center = this.view.WorldToViewportPoint(this.transform.position);
        this.deathPostProcess.angle = this.IsDying ? Mathf.Lerp(0.0f, 360.0f, (this.dyingTime % 1.0f) / 1.0f) : 0.0f;
        this.pausePostProcess.fadeWeight = Mathf.Lerp(this.pausePostProcess.fadeWeight, this.IsPaused ? 1.0f : 0.0f, Time.unscaledDeltaTime * 3.0f);

        for (int i = 0; i < this.pauseAndEndEffects.Length; ++i)
        {
            if (this.wave.IsEnded || this.IsPaused)
            {
                this.pauseAndEndEffects[i].PlayForward();
            }
            else
            {
                this.pauseAndEndEffects[i].PlayBackward();
            }
        }

        if (this.wave.IsEnded)
        {
            for (int i = 0; i < this.endEffects.Length; ++i)
            {
                this.endEffects[i].PlayForward();
            }

            this.endText.text = this.DeathCount + (this.DeathCount == 1 ? " Death" : " Deaths");
        }

        if (this.IsInvincible)
        {
            this.invincibleTime -= Time.deltaTime;
        }

        if (this.IsDying)
        {
            bool isDespawned = this.dyingTime >= 1.0f;

            this.dyingTime -= Time.unscaledDeltaTime;

            if (this.dyingTime < 1.0f && isDespawned)
            {
                this.transform.position = new Vector3(this.view.transform.position.x, -1.0f, 0.0f);
                this.transform.rotation = Quaternion.identity;

                for (int i = 0; i < this.despawnEffects.Length; ++i)
                {
                    this.despawnEffects[i].PlayBackward();
                }
            }
        }
        else
        {
            if (this.IsPaused && this.wave.IsGenerated && Input.GetButtonDown("Fire1"))
            {
                this.IsPaused = false;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Time.timeScale = 1.0f;
                Application.LoadLevel("Menu");
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                Time.timeScale = 1.0f;
                Application.LoadLevel(Application.loadedLevel);
            }

            if (!this.grapple.IsThrowing && !this.grapple.IsGrappling && Input.GetButtonDown("Fire1"))
            {
                Vector3 throwDirection = (Input.mousePosition - Camera.main.WorldToScreenPoint(this.transform.position)).normalized;

                this.grapple.Throw(throwDirection);
            }

            if (this.grapple.IsGrappling && Input.GetButton("Fire1"))
            {
                this.grapple.GrappleLength -= 5.0f * Time.deltaTime;
            }

            if (Input.GetButtonUp("Fire1"))
            {
                this.grapple.Retract();
            }

            Vector2 viewportPos = view.WorldToViewportPoint(this.transform.position);
            if (viewportPos.x < 0.0f || viewportPos.x > 1.0f || viewportPos.y < 0.0f || viewportPos.y > 1.0f)
            {
                this.Die(true);
            }
        }
    }
}
