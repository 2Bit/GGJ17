﻿using Slerpy.Unity3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splitter : MonoBehaviour {

    [SerializeField]
    private TransformEffect moveEffect;

    [SerializeField]
    private float waitTime = 2.0f;

    private float waitCompletion = 1.0f;

    private bool isWaiting = false;

    private int count = 1;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (waitCompletion < (isWaiting ? waitTime : moveEffect.Duration))
        {
            waitCompletion += Time.deltaTime;
        } else
        {
            waitCompletion = 0.0f;
            if (isWaiting)
            {
                moveEffect.Play();
                isWaiting = false;
            } else
            {
                count++;
                moveEffect.Stop();
                moveEffect.SimulatedTime = count;
                isWaiting = true;
            }
        }
	}
}
