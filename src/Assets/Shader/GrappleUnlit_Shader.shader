﻿Shader "Unlit/GrappleUnlit_Shader"
{
	Properties {
		[HDR] _Color ("Color", Color) = (1,1,1,1)
		_Emission ("Emission", Float) = 1.0
		_Thickness ("Line Thickness", Float) = 20.0
		_Sharpness ("Sharpness", Float) = .5
		_Amplitude ("Amplitude", Float) = 1.0
		_WaveLength ("WaveLength", Float) = .05
		_Velocity ("Velocity", Float) = 1.0
		_Clip ("Clip", Float) = .001
	}
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};
			
			struct fragmentInput {
				float4 position : SV_POSITION;
				float4 texcoord0 : TEXCOORD0;
			};

			fragmentInput vert(vertexInput i) {
				fragmentInput o;
				o.position = mul(UNITY_MATRIX_MVP, i.vertex);
				o.texcoord0 = i.texcoord0;
				return o;
			}

			float4 _Color;
			float _Emission;
			float _Thickness;
			float _Sharpness;
			float _WaveLength;
			float _Amplitude;
			float _Clip;
			float _Velocity;

			float wave(float2 position)
			{
				return pow(_Thickness / abs(position.y + sin((position.x * _WaveLength + _Time.y * _Velocity)) * _Amplitude * clamp(position.x, .15, .5)), _Sharpness);
			}


			fixed4 frag(fragmentInput i) : SV_Target{
				float4 color = _Color * _Emission;
				
				color.a = wave(-1.0 + 2.0 * i.texcoord0.xy);
				
				clip(color.a - _Clip);

				return color;
			}
			ENDCG
		}
	}
}
